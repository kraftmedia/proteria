<?php
namespace Kraft\Proteria\Controller\Webhook;

use Kraft\Proteria\Logger\Webhook\Logger;
use Kraft\Proteria\ShipmentStatuses;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Api\ShipmentRepositoryInterface;

class TrackingStatus extends AbstractWebhookAction
{
    const DELIVERED_SHIPMENT_STATUS = 'utlevert';

    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    public function __construct(
        ShipmentRepositoryInterface $shipmentRepository,
        Context $context,
        Logger $logger
    ) {
        $this->shipmentRepository = $shipmentRepository;

        parent::__construct($context, $logger);
    }

    public function execute()
    {
        $shipmentId = $this->getShipmentId();
        $shipmentStatus = $this->getShipmentStatus();

        if ($shipmentStatus === self::DELIVERED_SHIPMENT_STATUS) {
            $shipment = $this->shipmentRepository->get($shipmentId);

            $shipment->setData('proteria_status', ShipmentStatuses::DELIVERED);

            $this->shipmentRepository->save($shipment);
        }
    }

    private function getShipmentId()
    {
        return json_decode($this->_request->getContent())->shipmentid;
    }

    private function getShipmentStatus()
    {
        return json_decode($this->_request->getContent())->status;
    }
}