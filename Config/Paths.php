<?php

namespace Kraft\Proteria\Config;

final class Paths
{
    const API_KEY = 'proteria/general/api_key';
    const CUSTOMER_ID = 'proteria/general/customer_id';

    const MAGENTO_SHIPPING_METHODS = 'proteria/config/magento_shipping_methods';
    const API_SHIPPING_METHODS = 'proteria/config/api_shipping_methods';

    const SENDER_ADDRESS_LINE_1 = 'proteria/senders_address/line_1';
    const SENDER_ADDRESS_LINE_2 = 'proteria/senders_address/line_2';

    const DEFAULT_STORE_PACKAGE_WIDTH = 'proteria/config/package_width';
    const DEFAULT_STORE_PACKAGE_LENGTH = 'proteria/config/package_length';
    const DEFAULT_STORE_PACKAGE_HEIGHT = 'proteria/config/package_height';

    const AUTO_SUBMIT_SHIPMENTS = 'proteria/general/auto_submit';
    const AUTO_PRINT = 'proteria/general/cloud_print';

    const LABEL_PRINT_FORMAT = 'proteria/config/label_print_format';

    const SHIPPING_MAPPINGS_BASE_PATH = 'proteria/config/mapping_';

    const SANDBOX_MODE = 'proteria/general/sandbox_enabled';
}