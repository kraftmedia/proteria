# Proteria Integration for Magento

Proteria offers a number of software solutions for shipping and freight. This module integrates the Proteria Frakt service into Magento 2.

# Installation

### Via composer
To install this package with composer you need access to the command line of your server and you need to have Composer installed. Install with the following command series:

```
cd <your magento path>
composer require kraftmedia/proteria
php bin/magento module:enable Kraft_Proteria
php bin/magento setup:upgrade
php bin/magento cache:flush
```

If you have not set your composer stability you will get error: 
Problem 1
    - The requested package kraftmedia/proteria * is satisfiable by kraftmedia/proteria[dev-master] but these conflict with your requirements or minimum-stability.


### Manually
To install this package manually you need access to your server file system and you need access to the command line of your server. Take the following steps:

1. Download the zip file from the Bitbucket repository.
2. Upload the contents to `<your magento path>/app/code/Kraft/Proteria`.
3. Execute the following commands:

```
cd <your magento path>
composer require kraftmedia/proteria
php bin/magento module:enable Kraft_Proteria
php bin/magento setup:upgrade
php bin/magento cache:flush
```

## Contribute
If you find any issues with the module, please feel free to reach out to post@kraftmedia.no. Pull requests are welcome.

## Authors

- [Kraft Media](https://kraftmedia.no)

## About Kraft Media

Kraft Media provides top-tier solutions and support to Magento merchants. Learn more about us on our [website](https://kraftmedia.no).