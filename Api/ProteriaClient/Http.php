<?php
namespace Kraft\Proteria\Api\ProteriaClient;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Kraft\Proteria\Config\Paths;
use Kraft\Proteria\Logger\Sandbox\Logger;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Logger\Monolog;
use Psr\Http\Message\ResponseInterface;
use stdClass;

class Http
{
    /**
     * @var string
     */
    private $authorizationToken;

    /**
     * @var bool
     */
    private $isSandboxed;

    /**
     * @var Monolog
     */
    private $logger;

    public function __construct(ScopeConfigInterface $scopeConfig, Logger $logger)
    {
        $this->logger = $logger;
        $this->authorizationToken = $scopeConfig->getValue(Paths::API_KEY);
        $this->isSandboxed = (bool)$scopeConfig->getValue(Paths::SANDBOX_MODE);
    }

    public function post(string $url, stdClass $content): ResponseInterface
    {
        if ($this->isSandboxed) {
            $this->logger->addNotice(
                "Kraft_Proteria: request would be sent to {$url}, with body: \n\n{$this->getRequestBodyForContent($content)}\n\n"
            );

            return new Response();
        }

        $client = new Client();

        return $client->post($url, [
            'body' => $this->getRequestBodyForContent($content),
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $this->getAuthorizationToken()
            ]
        ]);
    }

    public function postSaveFile(string $url, stdClass $content, string $pathToSaveFile): ResponseInterface
    {
        if ($this->isSandboxed) {
            $this->logger->addNotice(
                "Kraft_Proteria: request would be sent to {$url}, with body: \n\n{$this->getRequestBodyForContent($content)}\n\n"
            );

            return new Response();
        }

        $client = new Client();

        return $client->post($url, [
            'body' => $this->getRequestBodyForContent($content),
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $this->getAuthorizationToken()
            ],
            'sink' => $pathToSaveFile
        ]);
    }

    private function getRequestBodyForContent(stdClass $content): string
    {
        return json_encode($content, JSON_UNESCAPED_SLASHES);
    }

    private function getAuthorizationToken(): string
    {
        return "Bearer {$this->authorizationToken}";
    }
}