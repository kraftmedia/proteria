<?php
namespace Kraft\Proteria\Api\ProteriaClient\Helpers\Extract;

class ReceiverObject extends AbstractExtractor implements ExtractorInterface
{
    public $requiredFields = [
        'Name',
        'PhoneNumber',
        'Address1',
        'PostCode',
        'City',
        'Country'
    ];

    protected function doExtraction()
    {
        $this->extractReceiverName();
        $this->extractReceiverEmail();
        $this->extractReceiverPhoneNumber();
        $this->extractReceiverAddressLine1();
        $this->extractReceiverAddressLine2();
        $this->extractReceiverPostCode();
        $this->extractReceiverCity();
        $this->extractReceiverCountry();
        $this->extractReceiverContact();
    }

    private function extractReceiverName()
    {
        $this->extraction->Name = $this->order->getBillingAddress()->getName();
    }

    private function extractReceiverEmail()
    {
        $this->extraction->Email = $this->order->getBillingAddress()->getEmail();
    }

    private function extractReceiverPhoneNumber()
    {
        $this->extraction->PhoneNumber = $this->order->getBillingAddress()->getTelephone();
    }

    private function extractReceiverAddressLine1()
    {
        $this->extraction->Address1 = $this->order->getBillingAddress()->getStreetLine(1);
    }

    private function extractReceiverAddressLine2()
    {
        $this->extraction->Address2 = $this->order->getBillingAddress()->getStreetLine(2);
    }

    private function extractReceiverPostCode()
    {
        $this->extraction->PostCode = $this->order->getBillingAddress()->getPostcode();
    }

    private function extractReceiverCity()
    {
        $this->extraction->City = $this->order->getBillingAddress()->getCity();
    }

    private function extractReceiverCountry()
    {
        $this->extraction->Country = $this->order->getBillingAddress()->getCountryId();
    }

    private function extractReceiverContact()
    {
        $contact = new \stdClass();

        $contact->Name = $this->extractReceiverName();
        $contact->Email = $this->extractReceiverEmail();
        $contact->PhoneNumber = $this->extractReceiverPhoneNumber();

        $this->extraction->Contact = $contact;
    }
}