<?php
namespace Kraft\Proteria\Logger\Webhook;

class HandlerWebhook extends \Magento\Framework\Logger\Handler\Base
{
    public $fileName = '/var/log/Proteria-Webhook.log';

    public $loggerType = Logger::DEBUG;
}