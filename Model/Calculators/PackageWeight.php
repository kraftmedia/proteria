<?php
namespace Kraft\Proteria\Model\Calculators;

use Magento\Sales\Model\Order\Shipment;

class PackageWeight
{
    /**
     * @var Shipment
     */
    private $shipment;

    public function setShipment(Shipment $shipment)
    {
        $this->shipment = $shipment;
    }

    public function calculate(): int
    {
        $totalWeight = 0;

        foreach ($this->shipment->getAllItems() as $item) {
            /** @var \Magento\Sales\Model\Order\Shipment\Item $item */
            $totalWeight += $item->getOrderItem()->getProduct()->getWeight() * $item->getQty();
        }

        return $totalWeight;
    }
}