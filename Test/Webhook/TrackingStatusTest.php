<?php
namespace Kraft\Proteria\Test\Webhook;

use Kraft\Proteria\Config\Webhooks;
use Kraft\Proteria\Controller\Webhook\TrackingStatus;
use Kraft\Proteria\ShipmentStatuses;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order\Shipment;
use PHPUnit\Framework\TestCase;
use Magento\Sales\Model\ResourceModel\Order\Shipment\Collection as ShipmentCollection;

class TrackingStatusTest extends TestCase
{
    /**
     * @magentoApiDataFixture Magento/Sales/_files/shipment.php
     */
    public function testShipmentProteriaStatusIsUpdatedWhenDeliveredValueIsPassed()
    {
        $objectManager = \Magento\TestFramework\Helper\Bootstrap::getObjectManager();
        $shipment = $this->getShipment($objectManager);
        $shipment->setData('proteria_status', ShipmentStatuses::SENT_TO_PROTERIA);
        $shipment->save();

        $requestData = [
            'shipmentid' => $shipment->getId(),
            'status' => TrackingStatus::DELIVERED_SHIPMENT_STATUS,
            'trackingnumber' => '12345',
            'trackingurl' => 'https://google.com'
        ];

        /** @var \GuzzleHttp\Client $client */
        $client = $objectManager->get(\GuzzleHttp\Client::class);
        /** @var UrlInterface $urlInterface */
        $urlInterface = $objectManager->get(UrlInterface::class);

        $client->post($urlInterface->getUrl(Webhooks::TRACKING_STATUS_WEBHOOK), [
            'body' => $this->getRequestBodyForContent($requestData),
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);

        $updatedShipment = $this->getShipment($objectManager);

        $this->assertEquals(ShipmentStatuses::DELIVERED, $updatedShipment->getData('proteria_status'), 'The shipment status has been changed to delivered.');
    }

    public function getRequestBodyForContent(array $content)
    {
        return json_encode($content, JSON_UNESCAPED_SLASHES);
    }

    /**
     * @param $objectManager
     * @return Shipment
     */
    private function getShipment($objectManager)
    {
        /** @var ShipmentCollection $shipmentCollection */
        $shipmentCollection = $objectManager->get(ShipmentCollection::class);

        return $shipmentCollection->getFirstItem();
    }
}