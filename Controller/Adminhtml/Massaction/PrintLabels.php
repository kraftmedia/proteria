<?php
namespace Kraft\Proteria\Controller\Adminhtml\Massaction;

use Kraft\Proteria\Api\ProteriaClient\Methods\GetLabel;
use Kraft\Proteria\Config\Paths;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Controller\Adminhtml\Shipment\AbstractShipment\PrintAction;
use Magento\Sales\Model\Order\Shipment;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Stdlib\DateTime\DateTime;

class PrintLabels extends PrintAction
{
    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    /**
     * @var GetLabel
     */
    private $getLabel;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var DateTime
     */
    private $dateTime;

    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        ForwardFactory $resultForwardFactory,
        ShipmentRepositoryInterface $shipmentRepository,
        GetLabel $getLabel,
        ScopeConfigInterface $scopeConfig,
        DateTime $dateTime
    ) {
        $this->shipmentRepository = $shipmentRepository;
        $this->getLabel = $getLabel;
        $this->scopeConfig = $scopeConfig;
        $this->dateTime = $dateTime;

        parent::__construct($context, $fileFactory, $resultForwardFactory);
    }

    /**
     * Based on \Magento\Sales\Controller\Adminhtml\Shipment\AbstractShipment\PrintAction::execute
     */
    public function execute()
    {
        if ($this->isSandboxed()) {
            $this->messageManager->addErrorMessage("Proteria labels cannot be retrieved in sandbox mode.");
            return $this->_redirect('sales/shipment');
        }

        /** @var Shipment $shipment */
        $shipmentIds = $this->getRequest()->getPost()->get('selected');
        if (is_array($shipmentIds)) {
            array_walk($shipmentIds, function ($shipmentId) {
                if ($this->shipmentRepository->get($shipmentId)) {
                    $this->getLabel->addShipmentId($shipmentId);
                }
            });

            $this->getLabel->getLabelForShipments();

            return $this->_fileFactory->create(
                $this->getFilename(),
                file_get_contents($this->getLabel->getPdfPath()),
                DirectoryList::VAR_DIR,
                'application/pdf'
            );
        } else {
            /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
            $resultForward = $this->resultForwardFactory->create();
            return $resultForward->forward('noroute');
        }
    }

    private function isSandboxed(): bool
    {
        return (bool)$this->scopeConfig->getValue(Paths::SANDBOX_MODE);
    }

    private function getFilename(): string
    {
        return "proteria_labels_{$this->dateTime->date('Y-m-d_H-i-s')}.pdf";
    }
}