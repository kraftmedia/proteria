<?php
namespace Kraft\Proteria\Model\Sales\Order\Shipment;

class Track extends \Magento\Sales\Model\Order\Shipment\Track
{
    public function setTrackUrl(string $url): Track
    {
        $this->setData('track_url', $url);

        return $this;
    }
}