<?php
namespace Kraft\Proteria\Api\ProteriaClient\Helpers\Extract;

use Kraft\Proteria\Config\Paths;

class SenderObject extends AbstractExtractor implements ExtractorInterface
{
    public $requiredFields = [
        'Name',
        'Address1',
        'PostCode',
        'City',
        'Country'
    ];

    protected function doExtraction()
    {
        $this->extractSenderName();
        $this->extractSenderEmail();
        $this->extractSenderPhoneNumber();
        $this->extractSenderAddressLine1();
        $this->extractSenderAddressLine2();
        $this->extractSenderPostCode();
        $this->extractSenderCity();
        $this->extractSenderCountry();
        $this->extractSenderContact();
    }

    private function extractSenderName()
    {
        $this->extraction->Name = $this->getSenderNameFromStoreConfig();
    }

    private function getSenderNameFromStoreConfig(): string
    {
        return $this->scopeConfig->getValue('general/store_information/name');
    }

    private function extractSenderEmail()
    {
        $this->extraction->Email = $this->getSenderEmailFromStoreConfig();
    }

    private function getSenderEmailFromStoreConfig(): string
    {
        return $this->scopeConfig->getValue('trans_email/ident_general/email');
    }

    private function extractSenderPhoneNumber()
    {
        $this->extraction->PhoneNumber = $this->getSenderPhoneNumberFromStoreConfig();
    }

    private function getSenderPhoneNumberFromStoreConfig(): string
    {
        return $this->scopeConfig->getValue('general/store_information/phone');
    }

    private function extractSenderAddressLine1()
    {
        $this->extraction->Address1 = $this->getSenderAddressLine1();
    }

    private function getSenderAddressLine1(): string
    {
        if (($proteriaLine1 = $this->scopeConfig->getValue(Paths::SENDER_ADDRESS_LINE_1))) {
            return $proteriaLine1;
        } else {
            return (string)$this->scopeConfig->getValue('shipping/origin/street_line1');
        }
    }

    private function extractSenderAddressLine2()
    {
        $this->extraction->Address2 = $this->getSenderAddressLine2();
    }

    private function getSenderAddressLine2(): string
    {
        if (($proteriaLine2 = $this->scopeConfig->getValue(Paths::SENDER_ADDRESS_LINE_2))) {
            return $proteriaLine2;
        } else {
            return (string)$this->scopeConfig->getValue('shipping/origin/street_line2');
        }
    }

    private function extractSenderPostCode()
    {
        $this->extraction->PostCode = $this->getSenderPostCode();
    }

    private function getSenderPostCode(): string
    {
        if (($proteriaZipCode = $this->scopeConfig->getValue('proteria/senders_address/zip'))) {
            return $proteriaZipCode;
        } else {
            return (string)$this->scopeConfig->getValue('shipping/origin/postcode');
        }
    }

    private function extractSenderCity()
    {
        return $this->extraction->City = $this->getSenderCity();
    }

    private function getSenderCity(): string
    {
        return (string)$this->scopeConfig->getValue('shipping/origin/city');
    }

    private function extractSenderCountry()
    {
         $this->extraction->Country = $this->getSenderCountry();
    }

    private function getSenderCountry(): string
    {
        return (string)$this->scopeConfig->getValue('shipping/origin/country_id');
    }

    private function extractSenderContact()
    {
        $this->extraction->ContactPerson = $this->getSenderContact();
    }

    private function getSenderContact(): \stdClass
    {
        $senderContact = new \stdClass();

        $senderContact->Name = $this->getSenderNameFromStoreConfig();
        $senderContact->Email = $this->getSenderEmailFromStoreConfig();
        $senderContact->PhoneNumber = $this->getSenderPhoneNumberFromStoreConfig();

        return $senderContact;
    }
}