<?php

namespace Kraft\Proteria\Block\Admin\System\Config\Frontend;

use GuzzleHttp\Client;
use Kraft\Proteria\Config\Paths;
use Kraft\Proteria\Model\Config\Source\ApiShippingMethods;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Form\Element\Multiselect;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Magento\Framework\Data\Form\ElementFactory;

class ParcelTypes extends Template implements RendererInterface
{
    protected $_template = 'Kraft_Proteria::config/parcelTypes.phtml';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var ElementFactory
     */
    private $elementFactory;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ElementFactory $elementFactory,
        Context $context,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->elementFactory = $elementFactory;

        parent::__construct($context, $data);
    }

    /**
     * Render element html
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        return $this->toHtml();
    }

    public function canShowInputs(): bool
    {
        return $this->scopeConfig->getValue(Paths::API_KEY) && $this->scopeConfig->getValue(Paths::CUSTOMER_ID);
    }

    public function generateMultiselectInput(array $data = []): Multiselect
    {
        return $this->elementFactory->create(Multiselect::class, $data);
    }

    public function getMagentoShippingMethodsInput(): Multiselect
    {
        $multiselect = $this->generateMultiselectInput([
            'name' => 'magento_shipping_methods',
            'values' => $this->getMagentoShippingMethods(),
            'value' => $this->getSelectedMagentoShippingMethods()
        ]);

        $multiselect->setForm($this->getData('form'));
        $multiselect->setId('proteria_config_magento_shipping_methods');

        return $multiselect;
    }

    public function getMagentoShippingMethods(): array
    {
        $carriers = $this->scopeConfig->getValue('carriers');
        $options = [];

        array_walk($carriers, function ($carrier, $carrierId) use (&$options) {
            $options[] = [
                'label' => $this->getCarrierLabel($carrier, $carrierId),
                'value' => $carrierId
            ];
        });

        return $options;
    }

    public function getSelectedMagentoShippingMethods(): array
    {
        $selectedMethods = $this->scopeConfig->getValue(Paths::MAGENTO_SHIPPING_METHODS);

        return explode(',', $selectedMethods);
    }

    public function getSelectedApiShippingMethods(): array
    {
        $selectedMethods = $this->scopeConfig->getValue(Paths::API_SHIPPING_METHODS);

        return explode(',', $selectedMethods);
    }

    private function getCarrierLabel(array $carrier, string $carrierId): string
    {
        if (isset($carrier['title'])) {
            return $carrier['title'];
        }

        if (isset($carrier['name'])) {
            return $carrier['name'];
        }

        return $carrierId;
    }

    public function getProteriaAPIAvailableShippingMethods(): array
    {
        return (new ApiShippingMethods())->toOptionArray();
    }

    public function getProteriaMethodForShippingMethod(string $shippingMethod): string
    {
        $return = (string)$this->scopeConfig->getValue(Paths::SHIPPING_MAPPINGS_BASE_PATH . $shippingMethod);

        return $return;
    }
}