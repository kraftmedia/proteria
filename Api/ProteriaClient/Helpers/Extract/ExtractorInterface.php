<?php
namespace Kraft\Proteria\Api\ProteriaClient\Helpers\Extract;

interface ExtractorInterface
{
    public function extract(): \stdClass;
}