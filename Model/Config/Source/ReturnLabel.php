<?php
namespace Kraft\Proteria\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class ReturnLabel implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 2, 'label' => __('No')], ['value' => 1, 'label' => __('Yes With Senders Address')], ['value' => 0, 'label' => __('Yes')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('Yes'), 1 => __('Yes With Senders Address'), 2 => __('No')];
    }
}
