<?php
namespace Kraft\Proteria\Api\ProteriaClient\Helpers\Extract;

class DeliveryObject extends AbstractExtractor implements ExtractorInterface
{
    public $requiredFields = [
        'Name',
        'PhoneNumber',
        'Address1',
        'PostCode',
        'City',
        'Country'
    ];

    protected function doExtraction()
    {
        $this->extractDeliveryIsCompany();
        $this->extractDeliveryName();
        $this->extractDeliveryEmail();
        $this->extractDeliveryPhoneNumber();
        $this->extractDeliveryAddressLine1();
        $this->extractDeliveryAddressLine2();
        $this->extractDeliveryPostCode();
        $this->extractDeliveryCity();
        $this->extractDeliveryCountry();
        $this->extractDeliveryContact();
    }

    private function extractDeliveryIsCompany()
    {
        $this->extraction->IsCompany = (bool)$this->order->getShippingAddress()->getCompany();
    }

    private function extractDeliveryName()
    {
        $this->extraction->Name = $this->order->getShippingAddress()->getName();
    }

    private function extractDeliveryEmail()
    {
        $this->extraction->Email = $this->order->getShippingAddress()->getEmail();
    }

    private function extractDeliveryPhoneNumber()
    {
        $this->extraction->PhoneNumber = $this->order->getShippingAddress()->getTelephone();
    }

    private function extractDeliveryAddressLine1()
    {
        $this->extraction->Address1 = $this->order->getShippingAddress()->getStreetLine(1);
    }

    private function extractDeliveryAddressLine2()
    {
        $this->extraction->Address2 = $this->order->getShippingAddress()->getStreetLine(2);
    }

    private function extractDeliveryPostCode()
    {
        $this->extraction->PostCode = $this->order->getShippingAddress()->getPostcode();
    }

    private function extractDeliveryCity()
    {
        $this->extraction->City = $this->order->getShippingAddress()->getCity();
    }

    private function extractDeliveryCountry()
    {
        $this->extraction->Country = $this->order->getShippingAddress()->getCountryId();
    }

    private function extractDeliveryContact()
    {
        $contact = new \stdClass();

        $contact->Name = $this->extraction->Name;
        $contact->Email = $this->extraction->Email;
        $contact->PhoneNumber = $this->extraction->PhoneNumber;

        $this->extraction->ContactPerson = $contact;
    }
}