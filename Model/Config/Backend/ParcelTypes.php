<?php

namespace Kraft\Proteria\Model\Config\Backend;

use Kraft\Proteria\Config\Paths;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;

class ParcelTypes extends Value
{
    /**
     * @var Http
     */
    private $request;

    /**
     * @var ConfigInterface
     */
    private $configResource;

    public function __construct(
        RequestInterface $request,
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        ConfigInterface $configResource,
        TypeListInterface $cacheTypeList,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->request = $request;
        $this->configResource = $configResource;

        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    public function beforeSave()
    {
        if ($this->request->getPost() && $this->shippingMappingsHaveChanged()) {
            $this->saveShippingMethodMappings();
        }

        $this->_dataSaveAllowed = false;

        return parent::beforeSave();
    }

    private function shippingMappingsHaveChanged(): bool
    {
        return $this->request->getPost()['groups']['config']['fields']['parcel_types_updated']['value'] === 'changed';
    }

    private function saveShippingMethodMappings()
    {
        $carriers = array_keys($this->_config->getValue('carriers'));

        foreach ($carriers as $carrier) {
            $this->configResource->saveConfig(
                Paths::SHIPPING_MAPPINGS_BASE_PATH . $carrier,
                $this->request->getPost($carrier . '_mapping'),
                $this->getData('scope'),
                $this->getData('scope_id')
            );
        }
    }
}
