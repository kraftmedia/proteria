<?php
namespace Kraft\Proteria\Api\ProteriaClient\Methods;

use Kraft\Proteria\Api\ProteriaClient\Http;
use Kraft\Proteria\Config\Paths;
use Kraft\Proteria\Config\Urls;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use \stdClass;

class GetLabel
{
    const METHOD_URL = 'label/GetLabel/';

    /**
     * @var Http
     */
    private $httpClient;

    /**
     * @var array
     */
    private $shipmentIds = [];

    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var DateTime
     */
    private $dateTime;

    private $pdfPath;

    public function __construct(
        Http $httpClient,
        ShipmentRepositoryInterface $shipmentRepository,
        ScopeConfigInterface $scopeConfig,
        DateTime $dateTime
    ) {
        $this->httpClient = $httpClient;
        $this->shipmentRepository = $shipmentRepository;
        $this->scopeConfig = $scopeConfig;
        $this->dateTime = $dateTime;

        $this->ensureLabelsDirectoryExists();
    }

    private function ensureLabelsDirectoryExists()
    {
        if (!file_exists(BP . '/var/proteria_labels')) {
            mkdir(BP . '/var/proteria_labels');
        }
    }

    public function setShipmentIds(array $shipmentIds)
    {
        $this->shipmentIds = $shipmentIds;
    }

    public function addShipmentId(int $shipmentId)
    {
        if (!in_array($shipmentId, $this->shipmentIds)) {
            $this->shipmentIds[] = $shipmentId;
        }
    }

    public function getLabelForShipments()
    {
        return $this->httpClient->postSaveFile($this->getUrl(), $this->getContent(), $this->getPdfPath());
    }

    private function getUrl(): string
    {
        return Urls::PROTERIA_BASE_URL . self::METHOD_URL . $this->getLabelFormatFromConfig();
    }

    private function getLabelFormatFromConfig(): string
    {
        return (string)$this->scopeConfig->getValue(Paths::LABEL_PRINT_FORMAT);
    }

    private function getContent(): stdClass
    {
        $content = new stdClass();
        $content->shipmentids = $this->shipmentIds;

        return $content;
    }

    public function getPdfPath(): string
    {
        if (!$this->pdfPath) {
            $this->pdfPath = BP . '/var/proteria_labels/' . $this->getCurrentDate() . '.pdf';
        }

        return $this->pdfPath;
    }

    private function getCurrentDate(): string
    {
        return $this->dateTime->date('Y-m-d_H-i-s');
    }
}