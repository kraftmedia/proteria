<?php
namespace Kraft\Proteria\Controller\Adminhtml\Massaction;

use Magento\Backend\App\Action;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Model\Order\Shipment;
use Kraft\Proteria\Exception\UnableToSubmitException;
use Kraft\Proteria\Config\Paths;

class SendShipments extends \Magento\Backend\App\Action
{
    /**
     * @var \Kraft\Proteria\Api\ProteriaClient\Methods\CreateShipment
     */
    private $api;

    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    /**
     * @var array
     */
    private $unsubmittedShipments = [];

    /**
     * @var array
     */
    private $submittedShipments = [];

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct(
        \Kraft\Proteria\Api\ProteriaClient\Methods\CreateShipment $api,
        ShipmentRepositoryInterface $shipmentRepository,
        ScopeConfigInterface $scopeConfig,
        Action\Context $context
    ) {
        $this->api = $api;
        $this->shipmentRepository = $shipmentRepository;
        $this->scopeConfig = $scopeConfig;

        parent::__construct($context);
    }

    public function execute()
    {
        if ($this->isSandboxed()) {
            $this->messageManager->addErrorMessage("Proteria shipments cannot be sent in sandbox mode.");
            return $this->_redirect('sales/shipment');
        }

        $shipmentIds = $this->getRequest()->getPost()->get('selected');
        if (is_array($shipmentIds)) {
            array_walk($shipmentIds, function ($shipmentId) {
                try {
                    $shipment = $this->shipmentRepository->get($shipmentId);

                    $this->submitShipmentToProteria($shipment);
                    $this->submittedShipments[] = $shipment->getIncrementId();
                } catch (\Exception $e) {
                    $this->unsubmittedShipments[] = $e;
                }
            });

            /** @var UnableToSubmitException $unsubmittedShipment */
            foreach ($this->unsubmittedShipments as $unsubmittedShipment) {
                $this->messageManager->addErrorMessage(__("Shipment {$unsubmittedShipment->getShipmentId()} could not be submitted because " . __($unsubmittedShipment->getReason())));
            }

            if (count($this->submittedShipments)) {
                if (count($this->submittedShipments) === 1) {
                    $this->messageManager->addSuccessMessage(__("Shipment {$this->getSubmittedShipmentIds()} was successfully submitted."));
                } else {
                    $this->messageManager->addSuccessMessage(__("Shipments {$this->getSubmittedShipmentIds()} were successfully submitted."));
                }
            }

            return $this->_redirect('sales/shipment');
        } else {
            /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
            $resultForward = $this->resultForwardFactory->create();
            return $resultForward->forward('noroute');
        }
    }

    private function isSandboxed(): bool
    {
        return (bool)$this->scopeConfig->getValue(Paths::SANDBOX_MODE);
    }

    private function getErroredShipmentIds(): string
    {
        $shipmentIds = '';

        /** @var UnableToSubmitException $unsubmittedShipment */
        foreach ($this->unsubmittedShipments as $unsubmittedShipment) {
            $shipmentIds .= "#{$unsubmittedShipment->getShipmentId()}, ";
        }

        return substr($shipmentIds, 0, strlen($shipmentIds) - 2);
    }

    private function getSubmittedShipmentIds(): string
    {
        $shipmentIds = '';

        /** @var UnableToSubmitException $submittedShipment */
        foreach ($this->submittedShipments as $submittedShipment) {
            $shipmentIds .= "#{$submittedShipment}, ";
        }

        return substr($shipmentIds, 0, strlen($shipmentIds) - 2);
    }

    private function submitShipmentToProteria(Shipment $shipment)
    {
        try {
            $order = $shipment->getOrder();

            $response = $this->api->setOrder($order)
                ->setShipment($shipment)
                ->createShipmentForOrder();

            if ($response['status'] === 200) {
                return;
            } else {
                throw new UnableToSubmitException($shipment->getIncrementId(), $response['reason']);
            }
        } catch (\Exception $e) {
            throw new UnableToSubmitException($shipment->getIncrementId(), $e->getMessage());
        }
    }
}