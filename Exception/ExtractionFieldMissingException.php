<?php
namespace Kraft\Proteria\Exception;

use Throwable;

class ExtractionFieldMissingException extends \Exception
{
    public function __construct(
        string $field,
        string $extractorClassName,
        Throwable $previous = null
    ) {
        parent::__construct(
            "The field $field was not found when running the extractor $extractorClassName",
            0,
            $previous
        );
    }
}