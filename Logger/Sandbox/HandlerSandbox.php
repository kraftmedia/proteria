<?php
namespace Kraft\Proteria\Logger\Sandbox;

use Monolog\Logger;

class HandlerSandbox extends \Magento\Framework\Logger\Handler\Base
{
    public $fileName = '/var/log/Proteria-Sandbox.log';

    public $loggerType = Logger::NOTICE;
}