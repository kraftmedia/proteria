<?php
namespace Kraft\Proteria\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->addProteriaStatusColumns($setup);
        }

        $installer->endSetup();
    }

    private function addProteriaStatusColumns(SchemaSetupInterface $setup)
    {
        $this->addProteriaStatusColumnToSalesShipment($setup);
        $this->addProteriaStatusColumnToSalesShipmentGrid($setup);
    }

    private function addProteriaStatusColumnToSalesShipment(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            'sales_shipment',
            'proteria_status',
            [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => false,
                'default' => 0,
                'comment' => "Proteria status"
            ]
        );
    }

    private function addProteriaStatusColumnToSalesShipmentGrid(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            'sales_shipment_grid',
            'proteria_status',
            [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => false,
                'default' => 0,
                'comment' => "Proteria status"
            ]
        );
    }
}