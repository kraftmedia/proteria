<?php
namespace Kraft\Proteria\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class ApiShippingMethods implements ArrayInterface
{
    /**
     * @var array
     */
    private $valueLabelArray = [];

    public function toOptionArray(): array
    {
        return [
            [
                'value' => '',
                'label' => __('--')
            ],
            [
                'value' => 1100,
                'label' => __('Climate-neutral service package')
            ],
            [
                'value' => 1101,
                'label' => __('Return Service Service Pack')
            ],
            [
                'value' => 1102,
                'label' => __('At the door')
            ],
            [
                'value' => 1105,
                'label' => __('Home delivery project')
            ],
            [
                'value' => 1106,
                'label' => __('business Package')
            ],
            [
                'value' => 1108,
                'label' => __('Return service Company package')
            ],
            [
                'value' => 1109,
                'label' => __('Business Package Express Over Night')
            ],
            [
                'value' => 1111,
                'label' => __('Return service Business package Express')
            ],
            [
                'value' => 1112,
                'label' => __('Posten\'s Pallelaster')
            ],
            [
                'value' => 1113,
                'label' => __('Business Package Multicolli')
            ],
            [
                'value' => 1114,
                'label' => __('Subscription-transport')
            ],
            [
                'value' => 1115,
                'label' => __('Cross Docking')
            ],
            [
                'value' => 1116,
                'label' => __('Cross Docking Express')
            ],
            [
                'value' => 1117,
                'label' => __('environmental Return')
            ],
            [
                'value' => 1120,
                'label' => __('mini Package')
            ],
            [
                'value' => 1121,
                'label' => __('CarryOn HomeShopping')
            ],
            [
                'value' => 1122,
                'label' => __('CarryOn HomeShopping Return')
            ],
            [
                'value' => 1123,
                'label' => __('CarryOn HomeShopping BulkSplit')
            ],
            [
                'value' => 1124,
                'label' => __('CarryOn HomeShopping BulkReturn')
            ],
            [
                'value' => 1126,
                'label' => __('CarryOn Business')
            ],
            [
                'value' => 1127,
                'label' => __('CarryOn Business Return')
            ],
            [
                'value' => 1128,
                'label' => __('CarryOn Business 09.00')
            ],
            [
                'value' => 1129,
                'label' => __('CarryOn Business Bulksplit 09.00')
            ],
            [
                'value' => 1130,
                'label' => __('CarryOn Business Bulk Split')
            ],
            [
                'value' => 1131,
                'label' => __('CarryOn Business Pallet')
            ],
            [
                'value' => 1132,
                'label' => __('CarryOn Business Pallet Return')
            ],
            [
                'value' => 1134,
                'label' => __('CarryOn Business Bulk Return')
            ],
            [
                'value' => 1137,
                'label' => __('CarryOn Business Pallet 09.00')
            ],
            [
                'value' => 1138,
                'label' => __('CarryOn Business EU Import')
            ],
            [
                'value' => 1139,
                'label' => __('CarryOn HomeShopping EU Import')
            ],
            [
                'value' => 1144,
                'label' => __('Package in mailbox with tracking')
            ],
            [
                'value' => 1145,
                'label' => __('Pack in the mailbox without tracking')
            ],
            [
                'value' => 1200,
                'label' => __('Piece of goods (domestic)')
            ],
            [
                'value' => 1201,
                'label' => __('Party Item (Domestic)')
            ],
            [
                'value' => 1202,
                'label' => __('Piece of goods (abroad)')
            ],
            [
                'value' => 1203,
                'label' => __('Airfreight')
            ],
            [
                'value' => 1204,
                'label' => __('Ocean Freight')
            ],
            [
                'value' => 1205,
                'label' => __('Supply Base Truck')
            ],
            [
                'value' => 1206,
                'label' => __('Supply Base Ship')
            ],
            [
                'value' => 1207,
                'label' => __('Partigods (overseas)')
            ],
            [
                'value' => 1208,
                'label' => __('Cargo Bulk Split')
            ],
            [
                'value' => 1209,
                'label' => __('Cargo Full Load')
            ],
            [
                'value' => 1210,
                'label' => __('Transportation')
            ],
            [
                'value' => 1300,
                'label' => __('Courier VIP')
            ],
            [
                'value' => 1301,
                'label' => __('Courier 1H')
            ],
            [
                'value' => 1302,
                'label' => __('Courier 2H')
            ],
            [
                'value' => 1303,
                'label' => __('Courier 4H')
            ],
            [
                'value' => 1304,
                'label' => __('Courier 6H')
            ],
            [
                'value' => 1305,
                'label' => __('Courier bicycle VIP')
            ],
            [
                'value' => 1306,
                'label' => __('Courier bicycle 1H')
            ],
            [
                'value' => 1307,
                'label' => __('Courier bicycle 2H')
            ],
            [
                'value' => 1308,
                'label' => __('Courier bicycle 4H')
            ],
            [
                'value' => 1309,
                'label' => __('Quickpack sameday')
            ],
            [
                'value' => 1310,
                'label' => __('Quickpack over night 0900')
            ],
            [
                'value' => 1311,
                'label' => __('Quickpack over night 1200')
            ],
            [
                'value' => 1312,
                'label' => __('Quickpack day certain')
            ],
            [
                'value' => 1313,
                'label' => __('Quickpack express economy')
            ],
            [
                'value' => 40,
                'label' => __('Schenker Stykkgods')
            ],
            [
                'value' => 41,
                'label' => __('Schenker Business Package')
            ],
            [
                'value' => 42,
                'label' => __('Schenker Partigods')
            ],
            [
                'value' => 43,
                'label' => __('Schenker Direct distribution')
            ],
            [
                'value' => 44,
                'label' => __('Schenker Express')
            ],
            [
                'value' => 70,
                'label' => __('Norwegian Shipping Letter')
            ],
            [
                'value' => 71,
                'label' => __('Norwegian Shipping Letter, Toten Transport')
            ],
            [
                'value' => 90,
                'label' => __('Bid Standard')
            ],
            [
                'value' => 91,
                'label' => __('Express Shipping Express')
            ],
            [
                'value' => 92,
                'label' => __('Bid delivery pallet')
            ],
            [
                'value' => 93,
                'label' => __('Bid delivery VIP')
            ],
            [
                'value' => 94,
                'label' => __('Auction Economy')
            ],
            [
                'value' => 95,
                'label' => __('Freight forwarding')
            ],
            [
                'value' => 96,
                'label' => __('Bike Bike')
            ],
            [
                'value' => 101,
                'label' => 	__('PostNord Partloads (party charter)')
            ],
            [
                'value' => 102,
                'label' => 	__('PostNord Groupage (piece goods)')
            ],
            [
                'value' => 103,
                'label' => 	__('PostNord DPD Innland')
            ],
            [
                'value' => 104,
                'label' => 	__('PostNord DPD Global')
            ],
            [
                'value' => 105,
                'label' => 	__('PostNord MyPack Collect')
            ],
            [
                'value' => 106,
                'label' => 	__('PostNord Return Drop Off')
            ],
            [
                'value' => 107,
                'label' => 	__('PostNord MyPack Home')
            ],
            [
                'value' => 108,
                'label' => 	__('PostNord MyPack Home Small')
            ],
            [
                'value' => 109,
                'label' => 	__('PostNord Express Next Day')
            ],
            [
                'value' => 110,
                'label' => 	__('PostNord Pallet')
            ],
            [
                'value' => 111,
                'label' => 	__('PostNord InNight')
            ],
            [
                'value' => 120,
                'label' => 	__('Mailing station inland')
            ],
            [
                'value' => 121,
                'label' => 	__('Postal station abroad')
            ],
            [
                'value' => 132,
                'label' => 	__('DHL Innland')
            ],
            [
                'value' => 150,
                'label' => 	__('Toten Transport Shipping Inland')
            ],
            [
                'value' => 151,
                'label' => 	__('Toten Transport Import')
            ],
            [
                'value' => 152,
                'label' => 	__('Toten Transport Export')
            ],
            [
                'value' => 153,
                'label' => 	__('Toten Transport Direct Oslo')
            ],
            [
                'value' => 154,
                'label' => 	__('Toten Transport Express')
            ],
            [
                'value' => 160,
                'label' => 	__('Ontime Logistics Stykkgods')
            ],
            [
                'value' => 161,
                'label' => 	__('Ontime Logistics Partigods')
            ],
            [
                'value' => 180,
                'label' => 	__('Eek Transport Stykkgods')
            ],
            [
                'value' => 181,
                'label' => 	__('Eek Transport Partigods')
            ],
            [
                'value' => 201,
                'label'	 => __('Nor Line\'s piece of goods')

            ],
            [
                 'value' => 202,
                 'label' => __('Nor Line Party Party')
            ],
            [
                'value' => 211,
                'label' => 	__('System-Transport pieceware')
            ],
            [
                'value' => 212,
                'label' => 	__('System-Transport Partigods')
            ],
            [
                'value' => 221,
                'label' => 	__('Best Transport Economy Bid Car')
            ],
            [
                'value' => 222,
                'label' => 	__('Best Transport Bid Bid Car')
            ],
            [
                'value' => 223,
                'label' => 	__('Best Transport Bid van')
            ],
            [
                'value' => 224,
                'label' => 	__('Best Transport Bid Truck')
            ],
            [
                'value' => 225,
                'label' => 	__('Best Transport Express courier')
            ],
            [
                'value' => 226,
                'label' => 	__('Best Transport Express van')
            ],
            [
                'value' => 227,
                'label' => 	__('Best Transport Express Truck')
            ],
            [
                'value' => 228,
                'label' => 	__('Best Transport Vip courier')
            ],
            [
                'value' => 229,
                'label' => 	__('Best Transport Vip van')
            ],
            [
                'value' => 230,
                'label' => 	__('Best Transport Distribution van')
            ],
            [
                'value' => 231,
                'label' => 	__('Best Transport Distribution Route')
            ],
            [
                'value' => 251,
                'label' => 	__('DSV piece of goods')
            ],
            [
                'value' => 252,
                'label' => 	__('DSV Partigods')
            ],
            [
                'value' => 301,
                'label' => 	__('Toms Transport Economy')
            ],
            [
                'value' => 302,
                'label' => 	__('Toms Transport Bud')
            ],
            [
                'value' => 303,
                'label' => 	__('Toms Transport Express')
            ],
            [
                'value' => 304,
                'label' => 	__('Toms Transport Vip')
            ],
            [
                'value' => 305,
                'label' => 	__('Toms Transport Truck')
            ],
            [
                'value' => 311,
                'label' => 	__('Oslo Bid service Bid 1 hour')
            ],
            [
                'value' => 312,
                'label' => 	__('Oslo Bidding Bid 2 hours')
            ],
            [
                'value' => 313,
                'label' => 	__('Oslo Bidding Bid 3 hours')
            ],
            [
                'value' => 314,
                'label' => 	__('Oslo Bidding Service Product 1 hour')
            ],
            [
                'value' => 315,
                'label' => 	__('Oslo Bidding Service Product 2 hours')
            ],
            [
                'value' => 316,
                'label' => 	__('Oslo Bidding Service Product 3 hours')
            ],
            [
                'value' => 317,
                'label' => 	__('Oslo Bidding Service Bidding Express')
            ],
            [
                'value' => 318,
                'label' => 	__('Oslo Courier Service Express')
            ],
            [
                'value' => 321,
                'label' => 	__('Speedy Distribution Small item')
            ],
            [
                'value' => 322,
                'label' => 	__('Speedy Distribution Item')
            ],
            [
                'value' => 323,
                'label' => 	__('Speedy Distribution Large Item')
            ],
            [
                'value' => 324,
                'label' => 	__('Speedy Distribution Small Truck')
            ],
            [
                'value' => 325,
                'label' => 	__('Speedy Distribution Truck at. 1')
            ],
            [
                'value' => 326,
                'label' => 	__('Speedy Distribution Truck at. 2')
            ],
            [
                'value' => 331,
                'label' => 	__('Skedsmo Bid and Goods')
            ],
            [
                'value' => 332,
                'label' => 	__('Skedsmo Bid and Product Truck')
            ],
            [
                'value' => 341,
                'label' => 	__('Swipe Standard')
            ],
            [
                'value' => 351,
                'label' => 	__('Bjørgs Budbil and Transport Distribution')
            ],
            [
                'value' => 352,
                'label' => 	__('Bjørgs Budbil and Transport Budbil')
            ],
            [
                'value' => 353,
                'label' => 	__('Bjørgs Budbil and Transport Varetur')
            ],
            [
                'value' => 354,
                'label' => 	__('Bjørgs Budbil and Transport Express')
            ],
            [
                'value' => 355,
                'label'	=> __('Bjørg\'s Budbil and Transport Vip')
            ],
            [
                'value' => 356,
                'label'	=> __('Bjørg\'s Budbil and Transport Pall')
            ],
            [
                'value' => 357,
                'label' => 	__('Bjørgs Budbil and Transport Truck')
            ],
            [
                'value' => 361,
                'label' => 	__('Fast-Boys Bid')
            ],
            [
                'value' => 362,
                'label' => 	__('Fast-Guess Product')
            ],
            [
                'value' => 363,
                'label' => 	__('Quick Guys Big item')
            ],
            [
                'value' => 364,
                'label' => 	__('Quick-Guy Express Bid')
            ],
            [
                'value' => 365,
                'label' => 	__('Quick-Guy Express Item')
            ],
            [
                'value' => 366,
                'label' => 	__('Quick-Guys Express Large item')
            ],
            [
                'value' => 367,
                'label' => 	__('Quick-Guys Truck')
            ],
            [
                'value' => 368,
                'label' => 	__('Quick-Guy Express Truck')
            ],
            [
                'value' => 369,
                'label' => 	__('Quick-Guy Distribution')
            ],
            [
                'value' => 371,
                'label' => 	__('Pilots Bid')
            ],
            [
                'value' => 372,
                'label' => 	__('Pilots Item')
            ],
            [
                'value' => 373,
                'label' => 	__('Pilots Great item')
            ],
            [
                'value' => 374,
                'label' => 	__('Pilot Timesbud')
            ],
            [
                'value' => 375,
                'label' => 	__('Pilots Times bid last')
            ],
            [
                'value' => 376,
                'label' => 	__('Pilots Express')
            ],
            [
                'value' => 377,
                'label' => 	__('Pilots Express Product')
            ],
            [
                'value' => 378,
                'label' => 	__('The Pilots Express are great')
            ],
            [
                'value' => 379,
                'label' => 	__('Pilots Truck')
            ],
            [
                'value' => 381,
                'label' => 	__('ColliCare Bud - Direct')
            ],
            [
                'value' => 382,
                'label' => 	__('ColliCare Express Bud - Direct')
            ],
            [
                'value' => 383,
                'label' => 	__('ColliCare Item - Direct')
            ],
            [
                'value' => 384,
                'label' => 	__('ColliCare Express Item - Direct')
            ],
            [
                'value' => 385,
                'label' => 	__('ColliCare Truck - Direct')
            ],
            [
                'value' => 386,
                'label' => 	__('ColliCare Tractor - Direct')
            ],
            [
                'value' => 387,
                'label' => 	__('ColliCare Container - Direct')
            ],
            [
                'value' => 388,
                'label' => 	__('ColliCare Direct Distribution - Direct')
            ],
            [
                'value' => 389,
                'label' => 	__('ColliCare Crane - Direct')
            ],
            [
                'value' => 390,
                'label' => 	__('ColliCare Piece of Goods - Distribution')
            ],
            [
                'value' => 391,
                'label' => 	__('ColliCare Party - Distribution')
            ],
            [
                'value' => 401,
                'label' => 	__('Ramberg Stykkgods')
            ],
            [
                'value' => 402,
                'label' => 	__('Ramberg Partigods')
            ],
            [
                'value' => 403,
                'label' => 	__('Ramberg Pall-priced goods')
            ],
            [
                'value' => 404,
                'label' => 	__('Ramberg Kolli-priced goods')
            ],
            [
                'value' => 411,
                'label' => 	__('Kirkestuen Stykkgods')
            ],
            [
                'value' => 421,
                'label' => 	__('Kjell Hansen Shipping Stuff goods')
            ],
            [
                'value' => 431,
                'label' => 	__('SR Transport piece goods')
            ],
            [
                'value' => 441,
                'label' => 	__('Xpressbud Bid')
            ],
            [
                'value' => 442,
                'label' => 	__('Xpressbud Express')
            ],
            [
                'value' => 443,
                'label' => 	__('Xpressbud Distribution')
            ],
            [
                'value' => 444,
                'label' => 	__('Xpressbud VIP')
            ],
            [
                'value' => 445,
                'label' => 	__('Xpressbud Item')
            ],
            [
                'value' => 461,
                'label' => 	__('Frode Pilskog Bud')
            ],
            [
                'value' => 462,
                'label' => 	__('Frode Pilskog Stykkgods')
            ],
            [
                'value' => 463,
                'label' => 	__('Frode Pilskog Express')
            ],
            [
                'value' => 464,
                'label' => 	__('Frode Pilskog Party')
            ],
            [
                'value' => 471,
                'label' => 	__('AtB Transport Budbil')
            ],
            [
                'value' => 472,
                'label' => 	__('AtB Transport vans')
            ],
            [
                'value' => 473,
                'label' => 	__('AtB Transport Large van')
            ],
            [
                'value' => 474,
                'label' => 	__('AtB Transport Truck')
            ],
            [
                'value' => 475,
                'label' => 	__('AtB Transport Economy')
            ],
            [
                'value' => 476,
                'label' => 	__('AtB Transport Hastebil')
            ],
            [
                'value' => 481,
                'label' => 	__('24/7 Express Route: Booking before 12:00')
            ],
            [
                'value' => 482,
                'label' => 	__('24/7 Express Ticket')
            ],
            [
                'value' => 483,
                'label' => 	__('24/7 Express vans')
            ],
            [
                'value' => 484,
                'label' => 	__('24/7 Express Express express car')
            ],
            [
                'value' => 485,
                'label' => 	__('24/7 Express Express van')
            ],
            [
                'value' => 486,
                'label' => 	__('24/7 Express Vip car immediate delivery')
            ],
            [
                'value' => 487,
                'label' => 	__('24/7 Express Truck')
            ],
            [
                'value' => 491,
                'label' => 	__('Singh Transport Budbil')
            ],
            [
                'value' => 492,
                'label' => 	__('Singh Transport vans')
            ],
            [
                'value' => 493,
                'label' => 	__('Singh Transport Large van')
            ],
            [
                'value' => 494,
                'label' => 	__('Singh Transport Truck')
            ],
            [
                'value' => 495,
                'label' => 	__('Singh Transport Large truck')
            ],
            [
                'value' => 496,
                'label' => 	__('Singh Transport Express express car')
            ],
            [
                'value' => 497,
                'label' => 	__('Singh Transport Express van')
            ],
            [
                'value' => 498,
                'label' => 	__('Singh Transport Express big van')
            ],
            [
                'value' => 499,
                'label' => 	__('Singh Transport Express truck')
            ],
            [
                'value' => 501,
                'label' => 	__('Budbilen Oslo Budbil')
            ],
            [
                'value' => 502,
                'label' => 	__('Budbilen Oslo Goods car')
            ],
            [
                'value' => 503,
                'label' => 	__('Budbilen Oslo Truck at. 1')
            ],
            [
                'value' => 504,
                'label' => 	__('Budbilen Oslo Truck at. 2')
            ],
            [
                'value' => 505,
                'label' => 	__('Budbilen Oslo Express')
            ],
            [
                'value' => 506,
                'label' => 	__('Budbilen Oslo Timesbil')
            ],
            [
                'value' => 511,
                'label' => 	__('Trobud Budbil')
            ],
            [
                'value' => 512,
                'label' => 	__('Trobud vans')
            ],
            [
                'value' => 513,
                'label' => 	__('Trobud vans w / rear lift')
            ],
            [
                'value' => 514,
                'label' => 	__('Trobud Truck w / rear lift')
            ],
            [
                'value' => 521,
                'label' => 	__('Pro Logistics Distribution')
            ],
            [
                'value' => 522,
                'label' => 	__('Pro Logistics Night Distribution')
            ],
            [
                'value' => 523,
                'label' => 	__('Proff Logistics Express')
            ],
            [
                'value' => 531,
                'label' => 	__('Kvikkas Budbil')
            ],
            [
                'value' => 541,
                'label' => 	__('RST Budbil')
            ],
            [
                'value' => 551,
                'label' => 	__('Transport service piece goods')
            ],
            [
                'value' => 561,
                'label' => 	__('SpedTrans piece of goods')
            ],
            [
                'value' => 571,
                'label' => 	__('Vennesla Transport piece goods')
            ],
            [
                'value' => 572,
                'label' => 	__('Vennesla Transport Partigods')
            ]
        ];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if (count($this->valueLabelArray) === 0) {
            foreach ($this->toOptionArray() as $option) {
                $valueLabelArray[$option['value']] = $option['label'];
            }
        }

        return $this->valueLabelArray;
    }
}