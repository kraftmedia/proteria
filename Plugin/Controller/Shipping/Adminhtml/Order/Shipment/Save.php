<?php
namespace Kraft\Proteria\Plugin\Controller\Shipping\Adminhtml\Order\Shipment;

use GuzzleHttp\Exception\RequestException;
use Kraft\Proteria\Api\ProteriaClient\Methods\CreateShipment;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\ShipmentRepository;
use Magento\Sales\Model\OrderRepository;

class Save
{
    /**
     * @var Http
     */
    private $request;

    /**
     * @var ShipmentRepository
     */
    private $shipmentRepository;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var Order
     */
    private $order;

    /**
     * @var CreateShipment
     */
    private $api;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    public function __construct(
        RequestInterface $request,
        ShipmentRepositoryInterface $shipmentRepository,
        OrderRepository $orderRepository,
        CreateShipment $api,
        ManagerInterface $messageManager
    ) {
        $this->request = $request;
        $this->shipmentRepository = $shipmentRepository;
        $this->orderRepository = $orderRepository;
        $this->api = $api;
        $this->messageManager = $messageManager;
    }

    public function afterExecute(\Magento\Shipping\Controller\Adminhtml\Order\Shipment\Save $subject, $result)
    {
        if ($this->request->getPost('proteria_submission')) {
            try {
                $this->submitShipmentToProteria();
                $this->messageManager->addSuccessMessage('The shipment was submitted to Proteria successfully.');
            } catch (RequestException $e) {
                $this->messageManager->addErrorMessage("The shipment could not be submitted to Proteria: {$e->getMessage()}");
            }
        }

        return $result;
    }

    private function submitShipmentToProteria()
    {
        $shipment = $this->getShipmentForOrder($this->request->getParam('order_id'));

        $this->api->setOrder($this->order)
            ->setShipment($shipment)
            ->createShipmentForOrder();
    }

    private function getShipmentForOrder(int $orderId): Order\Shipment
    {
        /** @var Order $order */
        $this->order = $this->orderRepository->get($this->request->getParam('order_id'));

        return $this->order->getShipmentsCollection()->setOrder('entity_id')->getFirstItem();
    }
}