<?php
namespace Kraft\Proteria\Ui\Component\Listing\Column\ProteriaStatus;

use Kraft\Proteria\ShipmentStatuses;

class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    public function toOptionArray(): array
    {
        return [
            [
                'value' => ShipmentStatuses::NOT_SENT,
                'label' => __('Not sent')
            ],
            [
                'value' => ShipmentStatuses::LABEL_READY,
                'label' => __('Label ready')
            ],
            [
                'value' => ShipmentStatuses::SENT_TO_PROTERIA,
                'label' => __('Sent to Proteria (waiting for label)')
            ],
            [
                'value' => ShipmentStatuses::FAILED,
                'label' => __('Failed')
            ],
            [
                'value' => ShipmentStatuses::DELIVERED,
                'label' => __('Delivered')
            ]
        ];
    }
}