<?php
namespace Kraft\Proteria\Api\ProteriaClient\Helpers\Extract;

use Kraft\Proteria\Exception\ExtractionFieldMissingException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order;

abstract class AbstractExtractor implements ExtractorInterface
{
    /**
     * @var \stdClass
     */
    public $extraction;

    /**
     * @var array
     */
    public $requiredFields = [];

    /**
     * @var Order
     */
    public $order;

    /**
     * @var ScopeConfigInterface
     */
    public $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @throws ExtractionFieldMissingException
     */
    public function validateExtraction()
    {
        foreach ($this->requiredFields as $requiredField) {
            if (!isset($this->extraction->{$requiredField})) {
                throw new ExtractionFieldMissingException($requiredField, self::class);
            }
        }
    }

    /**
     * @return \stdClass
     * @throws ExtractionFieldMissingException
     */
    final public function extract(): \stdClass
    {
        $this->extraction = new \stdClass();

        $this->doExtraction();
        $this->validateExtraction();

        return $this->extraction;
    }

    abstract protected function doExtraction();
}