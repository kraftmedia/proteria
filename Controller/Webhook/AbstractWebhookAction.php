<?php
namespace Kraft\Proteria\Controller\Webhook;

use Kraft\Proteria\Logger\Webhook\Logger;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

abstract class AbstractWebhookAction extends Action
{
    /**
     * @var Logger
     */
    protected $logger;

    public function __construct(Context $context, Logger $logger)
    {
        $this->logger = $logger;

        parent::__construct($context);
    }
}