<?php
namespace Kraft\Proteria\Controller\Webhook;

use Kraft\Proteria\Logger\Webhook\Logger;
use Kraft\Proteria\Model\Sales\Order\Shipment\Track;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Api\Data\ShipmentInterface;
use Magento\Sales\Api\Data\ShipmentTrackInterfaceFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Api\ShipmentTrackRepositoryInterface;
use Magento\Sales\Model\Order;

class BookingOk extends AbstractWebhookAction
{
    /**
     * @var Http
     */
    protected $_request;

    /**
     * @var ShipmentTrackRepositoryInterface
     */
    private $shipmentTrackRepository;

    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;

    /**
     * @var ShipmentTrackInterfaceFactory
     */
    private $shipmentTrackFactory;

    /**
     * @var ShipmentInterface
     */
    private $shipment;

    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Order
     */
    private $order;

    public function __construct(
        Context $context,
        ShipmentTrackInterfaceFactory $shipmentTrackFactory,
        ShipmentTrackRepositoryInterface $shipmentTrackRepository,
        ShipmentRepositoryInterface $shipmentRepository,
        OrderRepositoryInterface $orderRepository,
        JsonFactory $jsonResultFactory,
        Logger $logger
    ) {
        $this->shipmentTrackRepository = $shipmentTrackRepository;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->shipmentTrackFactory = $shipmentTrackFactory;
        $this->shipmentRepository = $shipmentRepository;
        $this->orderRepository = $orderRepository;

        parent::__construct($context, $logger);

        $this->getShipment();
    }

    private function getShipment()
    {
        $this->shipment = $this->shipmentRepository->get($this->getShipmentId());
    }

    private function getOrder()
    {
        if (!$this->order) {
            $this->order = $this->orderRepository->get($this->getOrderIdForShipment());
        }

        return $this->order;
    }

    public function execute()
    {
        $this->logger->addDebug('Proteria webhook ' . self::class . ' hit');

        try {
            /** @var Track $shipmentTrack */
            $shipmentTrack = $this->shipmentTrackFactory->create();

            $shipmentTrack->setParentId($this->getShipmentId());
            $shipmentTrack->setTrackNumber($this->getTrackingNumber());
            $shipmentTrack->setTrackUrl($this->getTrackingUrl());
            $shipmentTrack->setOrderId($this->getOrderIdForShipment());
            $shipmentTrack->setCarrierCode($this->getCarrierCodeForShipment());

            $this->shipmentTrackRepository->save($shipmentTrack);

            return $this->jsonResultFactory->create()
                ->setHttpResponseCode(200);
        } catch (\Exception $e) {
            return $this->jsonResultFactory->create(['error' => $e->getMessage()])
                ->setHttpResponseCode(500);
        }

    }

    private function getShipmentId(): int
    {
        return json_decode($this->_request->getContent())->shipmentid;
    }

    private function getTrackingNumber(): string
    {
        return json_decode($this->_request->getContent())->trackingnumber;
    }

    private function getTrackingUrl(): string
    {
        return json_decode($this->_request->getContent())->trackingurl;
    }

    private function getOrderIdForShipment(): int
    {
        return $this->shipment->getOrderId();
    }

    private function getCarrierCodeForShipment(): string
    {
        return $this->getOrder()->getShippingMethod(true)['carrier_code'];
    }
}