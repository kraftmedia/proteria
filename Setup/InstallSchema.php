<?php
namespace Kraft\Proteria\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $installer->getConnection()
            ->addColumn(
                'sales_shipment',
                'received_by_proteria',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Shipment received by Proteria'
                ]
            );

        $installer->getConnection()
            ->addColumn(
                'sales_shipment',
                'proteria_label_available',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Label available through Proteria'
                ]
            );

        $installer->getConnection()
            ->addColumn(
                'sales_shipment_track',
                'track_url',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Tracking URL'
                ]
            );

        $installer->endSetup();
    }
}