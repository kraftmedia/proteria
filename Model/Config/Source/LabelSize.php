<?php
namespace Kraft\Proteria\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class LabelSize implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 'A4', 'label' => __('A4')], ['value' => 'Label', 'label' => __('Label (optimized for thermal printers)')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return ['label' => __('Label (optimized for thermal printers)'), 'A4' => __('A4')];
    }
}
