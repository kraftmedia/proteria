<?php
namespace Kraft\Proteria\Controller\Webhook;

use Kraft\Proteria\Logger\Webhook\Logger;
use Kraft\Proteria\ShipmentStatuses;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Api\ShipmentRepositoryInterface;

class LabelAvailable extends AbstractWebhookAction
{
    /**
     * @var Http
     */
    protected $_request;

    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;

    public function __construct(
        Context $context,
        ShipmentRepositoryInterface $shipmentRepository,
        JsonFactory $jsonResultFactory,
        Logger $logger
    ) {
        $this->shipmentRepository = $shipmentRepository;
        $this->jsonResultFactory = $jsonResultFactory;

        parent::__construct($context, $logger);
    }

    public function execute()
    {
        $this->logger->addDebug('Proteria webhook ' . self::class . ' hit');

        try {
            $shipment = $this->shipmentRepository->get($this->getShipmentId());
            $shipment->setData('proteria_status', ShipmentStatuses::LABEL_READY);

            $this->shipmentRepository->save($shipment);

            return $this->jsonResultFactory->create()
                ->setHttpResponseCode(200);
        } catch (\Exception $e) {
            return $this->jsonResultFactory->create(['error' => $e->getMessage()])
                ->setHttpResponseCode(500);
        }

    }

    private function getShipmentId(): int
    {
        return json_decode($this->_request->getContent())->shipmentid;
    }
}