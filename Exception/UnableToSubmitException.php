<?php
namespace Kraft\Proteria\Exception;

class UnableToSubmitException extends \Exception
{
    /**
     * @var int
     */
    private $shipmentId;

    /**
     * @var string
     */
    private $reason;

    public function __construct($shipmentId, $reason)
    {
        $this->shipmentId = $shipmentId;
        $this->reason = $reason;

        parent::__construct();
    }

    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    public function getReason()
    {
        return $this->reason;
    }
}