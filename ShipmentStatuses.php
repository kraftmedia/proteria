<?php
namespace Kraft\Proteria;

class ShipmentStatuses
{
    const NOT_SENT = 0;
    const LABEL_READY = 1;
    const SENT_TO_PROTERIA = 2;
    const FAILED = 3;
    const DELIVERED = 4;
}