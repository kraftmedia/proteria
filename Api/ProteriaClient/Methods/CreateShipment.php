<?php
namespace Kraft\Proteria\Api\ProteriaClient\Methods;

use Kraft\Proteria\Api\ProteriaClient\Helpers\Extract\DeliveryObject;
use Kraft\Proteria\Api\ProteriaClient\Helpers\Extract\ExtractorFactory;
use Kraft\Proteria\Api\ProteriaClient\Helpers\Extract\SenderObject;
use Kraft\Proteria\Api\ProteriaClient\Http;
use Kraft\Proteria\Config\Paths;
use Kraft\Proteria\Config\Urls;
use Kraft\Proteria\Config\Webhooks;
use Kraft\Proteria\Model\Calculators\PackageWeight;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order;
use stdClass;

class CreateShipment
{
    const METHOD_URL = 'shipment/CreateShipments';

    /**
     * @var bool
     */
    private $autoSendShipments;

    /**
     * @var bool
     */
    private $autoPrint;

    /**
     * @var array
     */
    private $shipments = [];

    /**
     * @var Order
     */
    private $order;

    private $sender;

    private $delivery;

    private $shipment;

    private $returnTo;

    private $sendersReference = '';

    private $receiversReference = '';

    private $msgToReceiver = '';

    private $packageType;

    private $generateReturn = false;

    private $pickUpPointId = '';

    private $services = [];

    private $cashOnDelivery;

    private $parcels = [];

    private $orderId;

    private $extractors = [];

    private $requestObject;

    private $webhooks;

    private $httpClient;

    private $scopeConfig;

    /**
     * @var ExtractorFactory
     */
    private $extractorFactory;

    public function __construct(
        ExtractorFactory $extractorFactory,
        Webhooks $webhooks,
        Http $httpClient,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->extractorFactory = $extractorFactory;
        $this->webhooks = $webhooks;
        $this->httpClient = $httpClient;
        $this->scopeConfig = $scopeConfig;
    }

    public function setOrder(Order $order): CreateShipment
    {
        $this->order = $order;

        return $this;
    }

    public function setShipment(Order\Shipment $shipment): CreateShipment
    {
        $this->shipment = $shipment;

        return $this;
    }

    public function createShipmentForOrder(): array
    {
        $this->autoPrint = (bool)$this->scopeConfig->getValue(Paths::AUTO_PRINT);
        $this->autoSendShipments = (bool)$this->scopeConfig->getValue(Paths::AUTO_SUBMIT_SHIPMENTS);
        $this->packageType = $this->scopeConfig->getValue(Paths::SHIPPING_MAPPINGS_BASE_PATH . $this->getShippingCodeForOrder());

        $this->extractDeliveryFromOrder();
        $this->extractSenderFromOrder();
        $this->createShipmentForOrderObject();
        $this->buildRequestObject();

        $response = $this->sendRequest();

        return [
            'status' => $response->getStatusCode(),
            'reason' => $response->getReasonPhrase()
        ];
    }

    private function getShippingCodeForOrder(): string
    {
        return (string)$this->order->getShippingMethod(true)['carrier_code'];
    }

    private function sendRequest()
    {
        return $this->httpClient->post(Urls::PROTERIA_BASE_URL . self::METHOD_URL, $this->requestObject);
    }

    private function buildRequestObject()
    {
        $requestObject = new stdClass();

        $requestObject->AutoSendShipments = $this->autoSendShipments;
        $requestObject->AutoPrint = $this->autoPrint;
        $requestObject->Webhooks = $this->webhooks->getHooks();
        $requestObject->Shipments = $this->shipments;

        $this->requestObject = $requestObject;
    }

    private function createShipmentForOrderObject()
    {
        $shipmentObject = new stdClass();

        $shipmentObject->OrderId = $this->order->getId();
        $shipmentObject->ShipmentId = $this->shipment->getId();
        $shipmentObject->Sender = $this->sender;
        $shipmentObject->Delivery = $this->delivery;
        $shipmentObject->SendersReference = $this->sendersReference;
        $shipmentObject->ReceiversReference = $this->receiversReference;
        $shipmentObject->MsgToReceiver = $this->msgToReceiver;
        $shipmentObject->PackageType = $this->packageType;
        $shipmentObject->GenerateReturn = $this->generateReturn;
        $shipmentObject->PickUpPointId = $this->pickUpPointId;
        $shipmentObject->Parcels = $this->getParcelDefinitionForOrder();

        $this->shipments = [ $shipmentObject ];
    }

    private function getParcelDefinitionForOrder()
    {
        $parcel = new stdClass();
        $parcel->LengthCM = $this->getDefaultStorePackageLength();
        $parcel->WidthCM = $this->getDefaultStorePackageWidth();
        $parcel->HeightCM = $this->getDefaultStorePackageHeight();
        $parcel->WeightKG = $this->getWeightForOrder();
        $parcel->Reference = '';

        return [
            $parcel
        ];
    }

    private function getDefaultStorePackageLength(): int
    {
        return $this->scopeConfig->getValue(Paths::DEFAULT_STORE_PACKAGE_LENGTH);
    }

    private function getDefaultStorePackageWidth(): int
    {
        return $this->scopeConfig->getValue(Paths::DEFAULT_STORE_PACKAGE_WIDTH);
    }

    private function getDefaultStorePackageHeight(): int
    {
        return $this->scopeConfig->getValue(Paths::DEFAULT_STORE_PACKAGE_HEIGHT);
    }

    private function getWeightForOrder(): int
    {
        $calculator = new PackageWeight();

        $calculator->setShipment($this->shipment);

        return $calculator->calculate();
    }

    private function extractSenderFromOrder()
    {
        if (!isset($this->extractors[SenderObject::class])) {
            $this->extractors[SenderObject::class] = $this->extractorFactory->create(SenderObject::class);
        }

        $this->extractors[SenderObject::class]->setOrder($this->order);
        $this->sender = $this->extractors[SenderObject::class]->extract();
    }

    private function extractDeliveryFromOrder()
    {
        if (!isset($this->extractors[DeliveryObject::class])) {
            $this->extractors[DeliveryObject::class] = $this->extractorFactory->create(DeliveryObject::class);
        }

        $this->extractors[DeliveryObject::class]->setOrder($this->order);
        $this->delivery = $this->extractors[DeliveryObject::class]->extract();
    }
}