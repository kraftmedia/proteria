<?php
namespace Kraft\Proteria\Plugin\Sales;

use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Api\Data\ShipmentInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;

class ShipmentRepository
{
    private $writeConnection;

    public function __construct(ResourceConnection $writeConnection)
    {
        $this->writeConnection = $writeConnection;
    }

    public function afterSave(
        ShipmentRepositoryInterface $repositorySubject,
        ShipmentInterface $shipmentResult
    ): ShipmentInterface
    {
        $this->writeProteriaStatusToShipmentGridTable($shipmentResult);

        return $shipmentResult;
    }

    private function writeProteriaStatusToShipmentGridTable(ShipmentInterface $shipment)
    {
        $this->writeConnection->getConnection()->update('sales_shipment_grid', [
            'proteria_status' => $shipment->getData('proteria_status')
        ], "entity_id = {$shipment->getEntityId()}");
    }
}