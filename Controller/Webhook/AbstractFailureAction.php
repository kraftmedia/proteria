<?php
namespace Kraft\Proteria\Controller\Webhook;

use Kraft\Proteria\Logger\Webhook\Logger;
use Kraft\Proteria\ShipmentStatuses;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Framework\Controller\Result\JsonFactory;

abstract class AbstractFailureAction extends AbstractWebhookAction
{
    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;

    public function __construct(
        ShipmentRepositoryInterface $shipmentRepository,
        JsonFactory $jsonResultFactory,
        Context $context,
        Logger $logger
    ) {
        $this->shipmentRepository = $shipmentRepository;
        $this->jsonResultFactory = $jsonResultFactory;

        parent::__construct($context, $logger);
    }

    public function execute()
    {
        try {
            $shipment = $this->shipmentRepository->get($this->getShipmentId());
            $shipment->setData('proteria_status', ShipmentStatuses::FAILED);

            $this->shipmentRepository->save($shipment);

            return $this->jsonResultFactory->create()
                ->setHttpResponseCode(200);
        } catch (\Exception $e) {
            return $this->jsonResultFactory->create(['error' => $e->getMessage()])
                ->setHttpResponseCode(500);
        }

    }

    private function getShipmentId(): int
    {
        return json_decode($this->_request->getContent())->shipmentid;
    }
}