<?php
namespace Kraft\Proteria\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use stdClass;

class Webhooks
{
    const SHIPMENT_RECEIVED_WEBHOOK = 'proteria/webhook/shipmentReceived';
    const VALIDATION_STATUS_WEBHOOK = 'proteria/webhook/validationStatus';
    const BOOKING_OK_WEBHOOK = 'proteria/webhook/bookingOk';
    const BOOKING_FAILED_WEBHOOK = 'proteria/webhook/bookingFailed';
    const LABEL_AVAILABLE_WEBHOOK = 'proteria/webhook/labelAvailable';
    const TRACKING_STATUS_WEBHOOK = 'proteria/webhook/trackingStatus';
    const PRINT_FAILED_WEBHOOK = 'proteria/webhook/printFailed';

    private $baseSecureUrl;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->baseSecureUrl = $scopeConfig->getValue('web/secure/base_url');
    }

    public function getHooks(): stdClass
    {
        $hooks = new stdClass();

        $hooks->ShipmentReceived = $this->generateUrlForHook(self::SHIPMENT_RECEIVED_WEBHOOK);
        $hooks->LabelAvailable = $this->generateUrlForHook(self::LABEL_AVAILABLE_WEBHOOK);
        $hooks->BookingOk = $this->generateUrlForHook(self::BOOKING_OK_WEBHOOK);
//        $hooks->ValidationStatus = $this->generateUrlForHook(self::VALIDATION_STATUS_WEBHOOK);
        $hooks->BookingFailed = $this->generateUrlForHook(self::BOOKING_FAILED_WEBHOOK);
        $hooks->TrackingStatus = $this->generateUrlForHook(self::TRACKING_STATUS_WEBHOOK);
        $hooks->PrintFailed = $this->generateUrlForHook(self::PRINT_FAILED_WEBHOOK);

        return $hooks;
    }

    private function generateUrlForHook(string $webhook): string
    {
        return $this->baseSecureUrl . $webhook;
    }
}