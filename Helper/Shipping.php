<?php
namespace Kraft\Proteria\Helper;

use Magento\Framework\DataObject;
use Magento\Sales\Model\Order;

class Shipping extends \Magento\Shipping\Helper\Data
{
    /**
     * Retrieve tracking url with params
     *
     * @param  string $key
     * @param  \Magento\Sales\Model\Order|\Magento\Sales\Model\Order\Shipment|\Magento\Sales\Model\Order\Shipment\Track $model
     * @param  string $method Optional - method of a model to get id
     * @return string
     */
    protected function _getTrackingUrl($key, $model, $method = 'getId')
    {
        if ($this->modelTypeIsSupported($model) && $this->trackHasDirectUrl($model)) {
            return $this->getTrackingUrlForModel($model);
        } else {
            return parent::_getTrackingUrl($key, $model, $method);
        }
    }

    private function modelTypeIsSupported($model)
    {
        return in_array(get_class($model), [
            \Magento\Sales\Model\Order\Shipment::class,
            \Magento\Sales\Model\Order\Shipment\Track::class,
            \Kraft\Proteria\Model\Sales\Order\Shipment\Track::class
        ]);
    }

    private function trackHasDirectUrl(DataObject $model): bool
    {
        switch (get_class($model)) {
            case \Magento\Sales\Model\Order\Shipment::class:
                $model = $this->getTrackForShipment($model);
            case \Magento\Sales\Model\Order\Shipment\Track::class:
            case \Kraft\Proteria\Model\Sales\Order\Shipment\Track::class:
            default:
                /** @var \Magento\Sales\Model\Order\Shipment\Track $model */
                return (bool)$model->getData('track_url');

        }
    }

    private function getTrackingUrlForModel(DataObject $model): string
    {
        switch (get_class($model)) {
            case \Magento\Sales\Model\Order\Shipment::class:
                $model = $this->getTrackForShipment($model);
            case \Magento\Sales\Model\Order\Shipment\Track::class:
            default:
                /** @var \Magento\Sales\Model\Order\Shipment\Track $model */
                return $model->getData('track_url');

        }
    }

    private function getTrackForShipment(Order\Shipment $shipment): Order\Shipment\Track
    {
        return $shipment->getTracksCollection()->getFirstItem();
    }
}