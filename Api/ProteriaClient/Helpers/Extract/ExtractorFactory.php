<?php
namespace Kraft\Proteria\Api\ProteriaClient\Helpers\Extract;

use Magento\Framework\ObjectManagerInterface;

final class ExtractorFactory
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
    }

    public function create(string $extractorClass): AbstractExtractor
    {
        return $this->objectManager->create($extractorClass);
    }
}